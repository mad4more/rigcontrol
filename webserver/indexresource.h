#pragma once

// QtWebServer includes
#include "http/httpresource.h"

#include "systemstatuswebwidget.h"
#include "systemcontrolwebwidget.h"
#include "gpustatuswebwidget.h"
#include "minerlogwebwidget.h"

class IndexResource :
    public QtWebServer::Http::Resource {
public:
    IndexResource();
    ~IndexResource();

    void deliver(const QtWebServer::Http::Request& request,
                 QtWebServer::Http::Response& response);

private:
    SystemStatusWebWidget _systemStatusWebWidget;
    SystemControlWebWidget _systemControlWebWidget;
    GpuStatusWebWidget _gpuStatusWebWidget;
    MinerLogWebWidget _minerLogWebWidget;
};

