QT += core gui

TARGET = webserver
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    indexresource.cpp \
    amddisplaylibrary.cpp \
    systemstatus.cpp \
    systemstatuswebwidget.cpp \
    systemcontrolwebwidget.cpp \
    minerlogwebwidget.cpp \
    gpustatuswebwidget.cpp \
    mainweblayout.cpp \
    apiresource.cpp

include(../pods.pri)

HEADERS += \
    indexresource.h \
    amddisplaylibrary.h \
    systemstatus.h \
    systemstatuswebwidget.h \
    systemcontrolwebwidget.h \
    minerlogwebwidget.h \
    gpustatuswebwidget.h \
    mainweblayout.h \
    apiresource.h

RESOURCES += \
    resources.qrc

