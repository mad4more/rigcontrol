#include "systemstatus.h"

SystemStatus::SystemStatus() {
    sysinfo(&_sys_info);
}

int SystemStatus::uptime() {
    return _sys_info.uptime;
}

int SystemStatus::freeRAM() {
    return (_sys_info.freeram / 1048576);
}

int SystemStatus::totalRAM() {
    return (_sys_info.totalram / 1048576);
}

float SystemStatus::cpuLoad() {
    return (_sys_info.loads[0] * 100.0 / (float)(1 << SI_LOAD_SHIFT));
}

int SystemStatus::numberOfProcesses() {
    return (_sys_info.procs);
}
