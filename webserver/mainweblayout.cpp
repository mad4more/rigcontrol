
#include <QDomElement>

#include "mainweblayout.h"
#include "webwidget.h"

MainWebLayout::MainWebLayout()
    : QtWebServer::WebLayout() {
    addCss("/asset/bootstrap.css");

    addJs("/asset/jquery.js");
    addJs("/asset/bootstrap.js");
}

QString MainWebLayout::renderHtml(const QtWebServer::Http::Request& request) {
    QtWebServer::Html::Document document;
    document.setTitle(title());

    foreach(QString resource, _cssResources) {
        QDomElement linkNode = document.createElement("link");
        linkNode.setAttribute("href", resource);
        linkNode.setAttribute("rel", "stylesheet");
        linkNode.setAttribute("type", "text/css");
        document.head().appendChild(linkNode);
    }

    foreach(QString resource, _jsResources) {
        QDomElement scriptNode = document.createElement("script");
        scriptNode.setAttribute("src", resource);
        scriptNode.setAttribute("type", "text/javascript");

        // We have to put in this hack because self-closing script
        // tags are not allowed in HTML
        QDomText dummyText = document.createTextNode("");
        scriptNode.appendChild(dummyText);

        document.body().appendChild(scriptNode);
    }

    QDomElement updateScript = document.createElement("script");
    updateScript.setAttribute("type", "text/javascript");
    QDomText scriptText = document.createTextNode(
        "$(document).ready(function() {"
        "   $('.update').each(function(index, el) {"
        "       var $el = $(el);"
        "       var updateUrl = $el.data('api');"
        "       setInterval(function() {"
        "           $.get(updateUrl, function(data) {"
        "               $el.html(data);"
        "           });"
        "       }, 1000);"
        "   });"
        ""
        "   $('.ajax-button').on('click', function(e) {"
        "       e.preventDefault();"
        "       var url = $(this).data('api');"
        "       var method = $(this).data('method') || 'POST';"
        "       var formdata = $(this).data('formdata');"
        "       $.ajax({"
        "           url : url,"
        "           method: method,"
        "           data: formdata,"
        "           success: function (data) {"
        "           },"
        "           error: function (jXHR, textStatus, errorThrown) {"
        "               alert(errorThrown);"
        "           }"
        "       });"
        "   });"
        "});"
    );
    updateScript.appendChild(scriptText);
    document.body().appendChild(updateScript);

    QDomElement containerElement = document.createElement("div");
    //containerElement.setAttribute("class", "container");

    QDomElement logoElement = document.createElement("img");
    logoElement.setAttribute("src", "/asset/ethereum-logo.png");
    logoElement.setAttribute("width", "32px");
    logoElement.setAttribute("style", "margin: 0 auto; display: block;");
    containerElement.appendChild(logoElement);

    foreach(QtWebServer::WebWidget *webWidget, _webWidgets) {
        document.appendHtml(
            containerElement,
            webWidget->renderHtml(request)
        );
    }

    document.body().appendChild(containerElement);
    return document.toString();
}
