// QtWebServer includes
#include "util/utilformurlcodec.h"

// Own includes
#include <QProcess>

#include "indexresource.h"
#include "mainweblayout.h"

IndexResource::IndexResource() :
    QtWebServer::Http::Resource("/") {
}

IndexResource::~IndexResource() {
}

void IndexResource::deliver(const QtWebServer::Http::Request& request,
                            QtWebServer::Http::Response& response) {
    response.setStatusCode(QtWebServer::Http::Ok);
    response.setHeader(QtWebServer::Http::ContentType, "text/html");

    QProcess *process = new QProcess;
    process->start("cat /etc/hostname");
    process->waitForFinished();
    QString hostname = process->readAllStandardOutput();

    MainWebLayout layout;
    layout.setTitle(hostname);
    layout.addWebWidget(&_systemStatusWebWidget);
    layout.addWebWidget(&_systemControlWebWidget);
    layout.addWebWidget(&_gpuStatusWebWidget);
    layout.addWebWidget(&_minerLogWebWidget);

    response.setBody(layout.renderHtml(request).toUtf8());
}
