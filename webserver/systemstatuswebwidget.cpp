#include "systemstatuswebwidget.h"
#include "systemstatus.h"

#include <QDateTime>

SystemStatusWebWidget::SystemStatusWebWidget() {
}

QString SystemStatusWebWidget::renderHtml(const QtWebServer::Http::Request &request) {
    Q_UNUSED(request)
    return
        "<table class=\"table table-striped\">"
        "<tr>"
        "<th>CPU Load</th>"
        "<th>Number of processes</th>"
        "<th>Free RAM</th>"
        "<th>Total RAM</th>"
        "<th>Uptime</th>"
        "</tr>"
        "<tr>"
        "<td class='update' data-api='/api/cpuload'>-</td>"
        "<td class='update' data-api='/api/procs'>-</td>"
        "<td class='update' data-api='/api/freeram'>-</td>"
        "<td class='update' data-api='/api/totalram'>-</td>"
        "<td class='update' data-api='/api/uptime'>-</td>"
        "</tr>"
        "</table>";
}

