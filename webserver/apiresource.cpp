#include "apiresource.h"

#include "systemstatus.h"
#include "amddisplaylibrary.h"

#include <QProcess>

ApiResource::ApiResource() :
    QtWebServer::Http::Resource("/api/{resource}") {
    adl.update();
    startTimer(500);
}

ApiResource::~ApiResource() {
}

void ApiResource::deliver(const QtWebServer::Http::Request& request,
                            QtWebServer::Http::Response& response) {
    response.setStatusCode(QtWebServer::Http::Ok);
    response.setHeader(QtWebServer::Http::ContentType, "text/plain");

    QString result = "?";
    QMap<QString, QString> parameters = uriParameters(request.uniqueResourceIdentifier());

    SystemStatus systemStatus;

    if(parameters["resource"] == "cpuload") {
        result = QString("%1 %").arg(systemStatus.cpuLoad());
    }
    if(parameters["resource"] == "procs") {
        result = QString("%1").arg(systemStatus.numberOfProcesses());
    }
    if(parameters["resource"] == "freeram") {
        result = QString("%1 MB").arg(systemStatus.freeRAM());
    }
    if(parameters["resource"] == "totalram") {
        result = QString("%1 MB").arg(systemStatus.totalRAM());
    }
    if(parameters["resource"] == "uptime") {
        result = QDateTime::fromTime_t(systemStatus.uptime()).toUTC().toString("dd:hh:mm:ss");
    }

    if(parameters["resource"] == "minerlog") {
        QProcess *process = new QProcess;
        process->start("tail -n 15 /home/miner/tmp/miner.log");
        process->waitForFinished();
        result = process->readAllStandardOutput();
    }

    if(parameters["resource"] == "gpuinfo") {
        QMap<QString, QByteArray> params = request.urlParameters();
        int index = QString::fromUtf8(params["index"]).toInt();
        QString value = QString::fromUtf8(params["value"]);
        if(value == "cardname") {
            result = QString("%1").arg(adl.cardName(index));
        }
        if(value == "coreclock") {
            result = QString("%1MHz").arg(adl.coreClock(index));
        }
        if(value == "memoryclock") {
            result = QString("%1MHz").arg(adl.memoryClock(index));
        }
        if(value == "corevoltage") {
            result = QString("%1VDC").arg(adl.coreVoltage(index));
        }
        if(value == "performancelevel") {
            result = QString("L%1").arg(adl.performanceLevel(index));
        }
        if(value == "load") {
            result = QString("%1 %").arg(adl.gpuLoad(index), 3, 10, QChar('0'));
        }
        if(value == "fanspeed") {
            result = QString("%1 %").arg(adl.fanSpeed(index), 3, 10, QChar('0'));
        }
        if(value == "temperature") {
            result = QString("%1℃").arg(adl.temperature(index));
        }
        if(value == "powertune") {
            result = QString("%1 %").arg(adl.powertuneValue(index));
        }
    }

    response.setBody(result.toUtf8());
}

void ApiResource::timerEvent(QTimerEvent*) {
    adl.update();
}

